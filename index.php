<?php
/*
 * JP Meijers
 * 2017-03-07
 */

$geofences_file = "geofences.json";

function endpoints()
{
  if(isset($_REQUEST["dump"]))
  {
    debug_print_all();
  }
  else if(isset($_REQUEST["lat"]) and isset($_REQUEST["lon"]) )
  {
    //default behaviour is to dump closest 3 geofences
    closest_three();
  }
}

function sort_fences($a, $b)
{
  if($a->distance > $b->distance)
  {
    return 1;
  }
  else
  {
    return -1;
  }
}

function closest_three()
{
  global $geofences_file;
  $user_lat = $_REQUEST["lat"];
  $user_lon = $_REQUEST["lon"];

  $geofences_data = json_decode(file_get_contents($geofences_file));
  $geofences = $geofences_data->fences;

  foreach ($geofences as $fence)
  {
    $distance = haversineGreatCircleDistance($fence->lat, $fence->lon, $user_lat, $user_lon);
    $fence->distance = $distance;
  }

  usort($geofences, "sort_fences");

  $closest = array();
  $closest[] = $geofences[0];
  $closest[] = $geofences[1];
  $closest[] = $geofences[2];

  $closestFences = array();
  $closestFences["fences"] = $closest;
  return_data($closestFences);
}

function debug_print_all()
{
  global $geofences_file;
  $data;
  try
  {
    $geofences_data = file_get_contents($geofences_file);
    $data = json_decode($geofences_data);
    return_data($data);
  }
  catch(Exception $e)
  {
    return_error($e);
  }

}

function return_data($data)
{
  header('Content-Type: application/json');
  print(json_encode($data, JSON_PRETTY_PRINT));
}

function return_error($e)
{
  $data = array();
  $data["error"] = True;
  $data["error_message"] = $e->getMessage();
  header('Content-Type: application/json');
  print(json_encode($data, JSON_PRETTY_PRINT));
  die();
}

/**
 * Calculates the great-circle distance between two points, with
 * the Haversine formula.
 * @param float $latitudeFrom Latitude of start point in [deg decimal]
 * @param float $longitudeFrom Longitude of start point in [deg decimal]
 * @param float $latitudeTo Latitude of target point in [deg decimal]
 * @param float $longitudeTo Longitude of target point in [deg decimal]
 * @param float $earthRadius Mean earth radius in [m]
 * @return float Distance between points in [m] (same as earthRadius)
 */
function haversineGreatCircleDistance(
  $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
{
  // convert from degrees to radians
  $latFrom = deg2rad($latitudeFrom);
  $lonFrom = deg2rad($longitudeFrom);
  $latTo = deg2rad($latitudeTo);
  $lonTo = deg2rad($longitudeTo);

  $latDelta = $latTo - $latFrom;
  $lonDelta = $lonTo - $lonFrom;

  $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
    cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
  return $angle * $earthRadius;
}


endpoints();
?>
